import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { LandingComponent } from './landing/landing.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { FAQComponent } from './faq/faq.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactusComponent } from './contactus/contactus.component';

const routes: Routes = [
  { path: '', component: HomeComponent,
    children: [
      { path: '', component: LandingComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent, data: { title: 'Privacy Policy', toolbar: true } },
      { path: 'about', component: AboutUsComponent, data: { title: 'About', toolbar: true } },
      { path: 'faqs', component: FAQComponent, data: { title: 'FAQs', toolbar: true } },
      { path: 'contact', component: ContactusComponent, data: { title: 'Contact Us', toolbar: true } },
      { path: 'qr/:qrCode', component: HomeComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
