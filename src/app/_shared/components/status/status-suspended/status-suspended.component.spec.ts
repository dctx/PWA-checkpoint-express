import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusSuspendedComponent } from './status-suspended.component';

describe('StatusSuspendedComponent', () => {
  let component: StatusSuspendedComponent;
  let fixture: ComponentFixture<StatusSuspendedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusSuspendedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusSuspendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
