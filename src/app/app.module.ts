import { HttpClientModule } from '@angular/common/http';
import {BrowserModule, Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '@env/environment';
import { MaterialModule } from '@shared/modules/material/material.module';
import { ToolbarComponent } from '@shared/components/toolbar/toolbar.component';
import { TermsAndConditionComponent } from '@shared/components/terms-and-condition/terms-and-condition.component';
import { ConfirmationComponent } from '@shared/components/confirmation/confirmation.component';
import { FeedbackComponent } from './feedback/feedback.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    TermsAndConditionComponent,
    ConfirmationComponent,
    FeedbackComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
