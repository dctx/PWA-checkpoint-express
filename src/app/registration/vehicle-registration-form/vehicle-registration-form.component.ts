import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { StatusPendingComponent } from '@shared/components/status/status-pending/status-pending.component';
import { ApiService } from '@shared/services/api.service';
import { SelectWorkTypesComponent } from '@shared/components/select-work-types/select-work-types.component';
import { MatDialog } from '@angular/material/dialog';
import { AsyncValidators } from '@shared/validators/async-validators';
import { phoneNumberValidator } from '@shared/validators/phone.validator';
import { emailValidator } from '@shared/validators/email.validator';
import { nameFormatValidator } from '@shared/validators/name-format.validator';
import { nonNumericValidator } from '@shared/validators/non-numeric.validator';
import { nonEmptyValidator } from '@shared/validators/non-empty.validator';
import { addressValidator } from '@shared/validators/address.validator';
import { SelectIdTypesComponent } from '@shared/components/select-id-types/select-id-types.component';
import { TermsAndConditionComponent } from '@shared/components/terms-and-condition/terms-and-condition.component';
import { alphaNumericValidator } from '@shared/validators/alphanumeric.validator';
import { sameInputValidator } from '@shared/validators/same-field.validator';
import { ConfirmationComponent } from '@shared/components/confirmation/confirmation.component';
import { CanComponentDeactivate } from '@shared/services/can-deactivate.service';

@Component({
  selector: 'app-vehicle-registration-form',
  templateUrl: './vehicle-registration-form.component.html',
  styleUrls: ['./vehicle-registration-form.component.scss']
})
export class VehicleRegistrationFormComponent implements OnInit, CanComponentDeactivate {

  public regForm: FormGroup;

  public processing: boolean;

  public selectedWorkTypeLabel = '';

  public selectedIdTypeLabel = '';

  public tcAccepted: boolean;

  // TO DO: Write custom validator to note
  // exactly the minimum and maximum character lengths
  private errorMessages = {
    required: 'Required',
    minlength: 'Must be at least 6 characters',
    maxlength: 'Maximum length exceeded',
    phoneNumber: 'Not a valid mobile number',
    nonNumeric: 'Numbers are not allowed',
    alphaNumeric: 'Only letters and numbers are allowed',
    isSameLocation: 'Home address and destination location is the same',
    nonEmpty: 'Value should not be empty',
    email: 'Not a valid email'
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private asyncValidators: AsyncValidators,
  ) {
  }

  public ngOnInit(): void {
    this.regForm = this.formBuilder.group({
      aporType: ['', Validators.required],
      passType: 'VEHICLE',
      firstName: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        nameFormatValidator('First Name')
      ]],
      middleName: ['', [
        Validators.maxLength(50),
        nonEmptyValidator(),
        nonNumericValidator(),
        nameFormatValidator('Middle Name')
      ]],
      lastName: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        nonNumericValidator(),
        nameFormatValidator('Last Name')
      ]],
      suffix: ['', [
        Validators.maxLength(10),
        nonEmptyValidator(),
        nonNumericValidator(),
        nameFormatValidator('Name Suffix', true)
      ]],
      company: ['', [
        Validators.required,
        Validators.maxLength(100),
        nonEmptyValidator(),
        nameFormatValidator('Company')
      ]],
      idType: ['', Validators.required],
      identifierNumber: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        alphaNumericValidator('ID Number', false)
      ]],
      email: ['', [
        Validators.required,
        Validators.maxLength(100),
        Validators.email,
        emailValidator()
      ]],
      mobileNumber: ['', [
        Validators.required,
        phoneNumberValidator()
      ]],
      originName: ['Home Address', [
        Validators.required,
        nonEmptyValidator()
      ]],
      originStreet: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        addressValidator('Street'),
      ]],
      originCity: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        addressValidator('City'),
      ]],
      originProvince: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        addressValidator('Province'),
      ]],
      destName: ['Destination', [
        Validators.required,
        nonEmptyValidator()
      ]],
      destStreet: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        addressValidator('Street'),
      ]],
      destCity: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        addressValidator('City'),
      ]],
      destProvince: ['', [
        Validators.required,
        Validators.maxLength(50),
        nonEmptyValidator(),
        addressValidator('Province'),
      ]],
      plateNumber: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(11),
        nonEmptyValidator(),
        alphaNumericValidator('Plate Number'),
      ], [
        this.asyncValidators.existingReferenceId('Plate Number/Conduction Sticker')
      ]],
      remarks: ['', [
        Validators.maxLength(200)
      ]],
    }, {
      validators: sameInputValidator
    });
  }

  public upperCase($event): void {
    const value = $event.target.value;
    this.regForm.patchValue({
      plateNumber: value.toUpperCase()
    });
  }

  public btnSelectIdType(): void {
    const config = {
      minWidth: '98vw',
      height: '98vh',
      panelClass: 'full-screen-modal',
      data: { type: 'VEHICLE' }
    };
    const dialogRef = this.dialog.open(SelectIdTypesComponent, config);

    dialogRef.afterClosed().subscribe((selectedIdType: any) => {
      if (selectedIdType) {
        this.selectedIdTypeLabel = selectedIdType.label;
        this.regForm.controls.idType.setValue(selectedIdType.code);
      }
      this.regForm.controls.idType.markAsTouched();
    });
  }

  public btnSelectAporType(): void {
    const config = {
      minWidth: '98vw',
      height: '98vh',
      panelClass: 'full-screen-modal'
    };
    const dialogRef = this.dialog.open(SelectWorkTypesComponent, config);

    dialogRef.afterClosed().subscribe((selectedWorkType: any) => {
      if (selectedWorkType) {
        this.selectedWorkTypeLabel = selectedWorkType.label;
        this.regForm.controls.aporType.setValue(selectedWorkType.code);
      }
      this.regForm.controls.aporType.markAsTouched();
    });
  }

  public btnTermsAndCondition(event: MouseEvent): void {
    event.preventDefault();
    const config = {
      minWidth: '100%',
      height: '100%',
      panelClass: 'full-screen-modal'
    };
    const dialogRef = this.dialog.open(TermsAndConditionComponent, config);

    dialogRef.afterClosed().subscribe((accepted: boolean) => {
      if (accepted) {
        this.tcAccepted = accepted;
      }
    });
  }

  public async submit(): Promise<void> {

    // copy value from plateNumber to identifierNumber
    this.regForm.patchValue({
      identifierNumber: this.regForm.get('plateNumber').value
    });

    if (this.processing || this.regForm.pending) {
      return;
    }

    this.processing = true;

    if (this.regForm.valid) {
      await this.apiService.register(this.regForm.value)
        .then(() => this.showApplicationPendingDialog())
        .catch((e: HttpErrorResponse) => this.showErrorNotification(e));
    } else {
      this.snackBar.open('Some fields are invalid. Please review the form and try again.', 'Dismiss', { duration: 2000 });
      this.showFieldErrors(this.regForm);
    }

    this.processing = false;
  }

  public getError(controlName: string): string {
    const field = this.regForm.get(controlName);

    if (!field.touched) {
      return null;
    }

    const errorKeys = Object.keys(field.errors || {});
    if (!errorKeys.length) {
      return null;
    }

    return field.errors[errorKeys[0]].message || this.errorMessages[errorKeys[0]];
  }

  private showFieldErrors(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.showFieldErrors(control as FormGroup);
      }
    });
  }

  private showApplicationPendingDialog(): void {
    this.regForm.reset();
    const dialogConfig = {
      minWidth: '100vw',
      height: '100vh',
      panelClass: 'full-screen-modal',
      data: { showApplySuccess: true }
    };
    const dialog = this.dialog.open(StatusPendingComponent, dialogConfig);
    dialog.beforeClosed().toPromise().then((wentHome: boolean) => {
      if (!wentHome) {
        this.router.navigateByUrl('onboarding/reminders');
      }
    });
  }

  private showErrorNotification(errorResponse: HttpErrorResponse): void {
    let errorMessage = 'Something went wrong. Please Try Again.';
    if (errorResponse?.status === 400 && errorResponse?.error?.message) {
      errorMessage = errorResponse.error.message;
    }
    this.snackBar.open(errorMessage, 'Dismiss', { duration: 5000 });
  }

  public async canDeactivate(): Promise<boolean> {
    if (this.regForm.dirty) {
      return await this.unsavedChanges();
    }

    return true;
  }

  public async unsavedChanges(): Promise<boolean> {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '450px'
    });
    return await dialogRef.afterClosed().toPromise();
  }

}
