import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccessPassModel } from '@shared/models/access-pass.model';

@Component({
  selector: 'app-status-expired',
  templateUrl: './status-expired.component.html',
  styleUrls: ['./status-expired.component.scss']
})
export class StatusExpiredComponent implements OnInit {

  public data: AccessPassModel;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any) {
    this.data = this.dialogData.result;
  }

  public ngOnInit(): void {
  }

}
