import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-select-id-types',
  templateUrl: './select-id-types.component.html',
  styleUrls: ['./select-id-types.component.scss']
})
export class SelectIdTypesComponent implements OnInit {

  public searchKey: string;

  public filteredItems: any;

  public idTypes: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public dialogRef: MatDialogRef<SelectIdTypesComponent>
  ) {
  }

  public ngOnInit(): void {
    if (this.data.type === 'VEHICLE') {
      this.getVehicleIdTypes();
    } else {
      this.getIndividualIdTypes();
    }
    this.onFilterItem(''); // when you fetch collection from server.
  }

  public onFilterItem(value: any) {
    if (!value) {
      this.filteredItems = this.idTypes;
    } // when nothing has typed
    this.filteredItems = this.idTypes.filter(item => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1);
  }

  public btnSelectThis(idType: any) {
    const selectedIdType = {
      code: idType.code,
      label: idType.label
    };
    this.dialogRef.close(selectedIdType);
  }

  private getVehicleIdTypes() {
    this.idTypes = [
      {
        code: 'PLT',
        label: 'Plate Number'
      },
      {
        code: 'CND',
        label: 'Conduction Sticker'
      }
    ];
  }

  private getIndividualIdTypes() {
    this.idTypes = [
      {
        code: 'COM',
        label: 'Company ID'
      }, {
        code: 'DPL',
        label: 'Diplomat ID'
      }, {
        code: 'LTO',
        label: 'Driver\'s License'
      }, {
        code: 'GSIS',
        label: 'GSIS/UMID'
      }, {
        code: 'NBI',
        label: 'NBI Clearance'
      }, {
        code: 'OFW',
        label: 'OFW'
      }, {
        code: 'PGB',
        label: 'Pag-IBIG'
      }, {
        code: 'DFA',
        label: 'Passport'
      }, {
        code: 'PHC',
        label: 'PhilHealth'
      }, {
        code: 'POS',
        label: 'Postal'
      }, {
        code: 'PRC',
        label: 'PRC'
      }, {
        code: 'PWD',
        label: 'PWD'
      }, {
        code: 'SNR',
        label: 'Senior Citizen'
      }, {
        code: 'SSS',
        label: 'SSS/UMID'
      }, {
        code: 'BIR',
        label: 'TIN'
      }, {
        code: 'CML',
        label: 'Voter\'s ID'
      }
    ];
  }

}
