export const environment = {
  production: true,
  apiUrl: window["env"]["apiUrl"] || 'https://rapidpass-api.azurewebsites.net',
  debug: window["env"]["debug"] || false
};
