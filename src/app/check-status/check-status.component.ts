import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ApiService } from '@shared/services/api.service';
import { StatusInvalidDialogComponent } from './status-invalid-dialog/status-invalid-dialog.component';
import { StatusPendingComponent } from '@shared/components/status/status-pending/status-pending.component';
import { StatusApprovedComponent } from '@shared/components/status/status-approved/status-approved.component';
import { StatusDeniedComponent } from '@shared/components/status/status-denied/status-denied.component';
import { StatusExpiredComponent } from '@shared/components/status/status-expired/status-expired.component';
import { StatusSuspendedComponent } from '@shared/components/status/status-suspended/status-suspended.component';

@Component({
  selector: 'app-check-status',
  templateUrl: './check-status.component.html',
  styleUrls: ['./check-status.component.scss']
})
export class CheckStatusComponent implements OnInit {
  checkStatusForm: FormGroup;
  processing: boolean;

  constructor(
    public router: Router,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
  ) {

    this.processing = false;
  }

  ngOnInit(): void {
    this.checkStatusForm = this.formBuilder.group({
      plateMobileNumber: ['', Validators.required]
    });
  }

  async submit(): Promise<void> {
    this.processing = true;
    try {
      const referenceId = this.checkStatusForm.value.plateMobileNumber.trim();
      const result = await this.apiService.checkStatus(referenceId);
      const fullScreenConfig = {
        minWidth: '100%',
        height: '100%',
        panelClass: 'full-screen-modal',
        data: { result },
      };

      if (result.status === 'PENDING') {
        this.dialog.open(StatusPendingComponent, fullScreenConfig);    // PENDING
      } else if (result.status === 'APPROVED') {
        const isNotExpired = new Date() < new Date(result.validUntil);
        if (isNotExpired) {
          this.dialog.open(StatusApprovedComponent, fullScreenConfig); // APPROVED
        } else {
          this.dialog.open(StatusExpiredComponent, fullScreenConfig);  // EXPIRED
        }
      } else if (result.status === 'DECLINED') {
        this.dialog.open(StatusDeniedComponent, fullScreenConfig);     // DECLINED
      } else if (result.status === 'SUSPENDED') {
        this.dialog.open(StatusSuspendedComponent, fullScreenConfig);  // SUSPENDED
      }
    } catch (e) {
      this.dialog.open(StatusInvalidDialogComponent, {
        width: '250px'
      });
    }
    this.processing = false;
  }

  get plateMobileNumberErrorMsg() {
    const plateMobileNumber = this.checkStatusForm.controls.plateMobileNumber;
    if (!plateMobileNumber.touched) {
      return '';
    }

    return plateMobileNumber.hasError('required') ? 'Required' : '';
  }

}
