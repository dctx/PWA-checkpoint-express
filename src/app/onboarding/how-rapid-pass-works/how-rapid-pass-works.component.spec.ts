import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowRapidPassWorksComponent } from './how-rapid-pass-works.component';

describe('HowRapidPassWorksComponent', () => {
  let component: HowRapidPassWorksComponent;
  let fixture: ComponentFixture<HowRapidPassWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowRapidPassWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowRapidPassWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
