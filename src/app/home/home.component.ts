import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const qrCode = this.route.snapshot.paramMap.get('qrCode');
    if (qrCode) {
      document.location.href = `${ environment.apiUrl }/api/v1/registry/qr-codes/${ qrCode }`;
      this.router.navigateByUrl('', { replaceUrl: true });
    }
  }

}
