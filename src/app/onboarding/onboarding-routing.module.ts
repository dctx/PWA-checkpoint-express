import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnboardingComponent } from './onboarding.component';
import { HowRapidPassWorksComponent } from './how-rapid-pass-works/how-rapid-pass-works.component';
import { RemindersComponent } from './reminders/reminders.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/',
    component: OnboardingComponent,
    children: [
      { path: '', redirectTo: '/',component: HowRapidPassWorksComponent, data: { toolbar: true} },
      { path: 'reminders', redirectTo: '/',component: RemindersComponent, data: { toolbar: true} },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardingRoutingModule { }
