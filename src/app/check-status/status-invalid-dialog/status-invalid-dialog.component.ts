import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-status-invalid-dialog',
  templateUrl: './status-invalid-dialog.component.html',
  styleUrls: ['./status-invalid-dialog.component.scss']
})
export class StatusInvalidDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<StatusInvalidDialogComponent>) { }

  ngOnInit(): void {
  }

}
