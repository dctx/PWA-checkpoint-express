export interface AccessPassModel {
  passType: string;
  aporType: string;
  referenceId: string;
  controlCode: string;
  name: string;
  company: string;
  idType: string;
  identifierNumber: string;
  destStreet: string;
  destCity: string;
  destProvince: string;
  status: string;
  validFrom: Date;
  validUntil: Date;
  remarks: string;
  reason: string;
}
