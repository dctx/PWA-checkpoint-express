import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AccessPassModel } from '@shared/models/access-pass.model';
import { RapidPassRequestModel } from '@shared/models/rapid-pass-request.model';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {
  }

  public async register(payload: RapidPassRequestModel): Promise<AccessPassModel> {
    return this.httpClient
      .post<AccessPassModel>(`${environment.apiUrl}/api/v1/registry/access-passes`, payload)
      .toPromise();
  }

  public async checkStatus(referenceId: string): Promise<AccessPassModel> {
    return this.httpClient
      .get<AccessPassModel>(`${environment.apiUrl}/api/v1/registry/access-passes/status/${referenceId}`)
      .toPromise();
  }

  public async getQrImage(referenceId: string): Promise<any> {
    return this.httpClient
      .get(`${environment.apiUrl}/api/v1/registry/qr-codes/${referenceId}/qr-code`, { responseType: 'text' })
      .toPromise();
  }

  /**
   * TO DO: Fetch work types from backend (no endpoint for that yet).
   */
  public getWorkTypes() {
    return [
      {
        code: 'AG',
        label: 'Agribusiness & Agricultural Workers'
      }, {
        code: 'BA',
        label: 'Banks'
      }, {
        code: 'BP',
        label: 'BPOs & Export-Oriented Business Personnel'
      }, {
        code: 'CA',
        label: 'Civil Aviation'
      }, {
        code: 'CB',
        label: 'Customs Brokerage (Logistics Services)'
      }, {
        code: 'CM',
        label: 'Capital Market Personnel (Skeletal Workforce)'
      }, {
        code: 'CY',
        label: 'Container Yards (Logistics Services)'
      }, {
        code: 'DE',
        label: 'Delivery Services'
      }, {
        code: 'DO',
        label: 'Distressed OFWs'
      }, {
        code: 'DP',
        label: 'Delivery of Parcels'
      }, {
        code: 'DS',
        label: 'DOLE Skeletal Staff for TUPAD'
      }, {
        code: 'EN',
        label: 'Energy Companies'
      }, {
        code: 'ER',
        label: 'Emergency Responders'
      }, {
        code: 'FC',
        label: 'Food Chain / Restaurants'
      }, {
        code: 'FF',
        label: 'Freight Forwarders (Logistics Services)'
      }, {
        code: 'FS',
        label: 'Funeral Service'
      }, {
        code: 'GE',
        label: 'Government Agency - Executive'
      }, {
        code: 'GJ',
        label: 'Government Agency - Judicial'
      }, {
        code: 'GL',
        label: 'Government Agency - Legislative'
      }, {
        code: 'GR',
        label: 'Grocery / Convenience Stores (Retail Establishments)'
      }, {
        code: 'HM',
        label: 'Heads of Mission / Designated Foreign Mission Reps'
      }, {
        code: 'HT',
        label: 'Hotel Employees and Tenants'
      }, {
        code: 'IP',
        label: 'International Passengers and Driver'
      }, {
        code: 'ME',
        label: 'Media Personalities'
      }, {
        code: 'MS',
        label: 'Medical Services'
      }, {
        code: 'MF',
        label: 'Manufacturing'
      }, {
        code: 'MT',
        label: 'Money Transfer Services'
      }, {
        code: 'PH',
        label: 'Pharmacies / Drug Stores'
      }, {
        code: 'PM',
        label: 'Public Market'
      }, {
        code: 'PO',
        label: 'Port Operators (Logistics Services)'
      }, {
        code: 'SA',
        label: 'Sanitation'
      }, {
        code: 'SH',
        label: 'Ship Captain & Crew'
      }, {
        code: 'SL',
        label: 'Shipping Lines (Logistics Services)'
      }, {
        code: 'SS',
        label: 'Security Services'
      }, {
        code: 'TE',
        label: 'Telecommunications'
      }, {
        code: 'TF',
        label: 'Transportation Facilities'
      }, {
        code: 'TS',
        label: 'Trucking Services (Logistics Services)'
      }, {
        code: 'VE',
        label: 'Veterinary'
      }, {
        code: 'WC',
        label: 'Water Companies'
      }, {
        code: 'WH',
        label: 'Warehousing (Logistics Services)'
      }
    ];
  }
}
