import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-status-pending',
  templateUrl: './status-pending.component.html',
  styleUrls: ['./status-pending.component.scss']
})
export class StatusPendingComponent implements OnInit {

  public showApplySuccess: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: any,
    private router: Router,
    private dialogRef: MatDialogRef<StatusPendingComponent>
  ) {
    this.showApplySuccess = !!this.dialogData.showApplySuccess;
  }

  public ngOnInit(): void {
  }

  goHome() {
    this.router.navigate([''], {replaceUrl: true});
    this.dialogRef.close(true); // true if nav to home
  }

}
