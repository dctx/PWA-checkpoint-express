import {AbstractControl , ValidationErrors, ValidatorFn} from '@angular/forms';

export const sameInputValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    const userIntentAddressCharCount = 3;

    const originStreet = control.get('originStreet');
    const originCity = control.get('originCity');
    const originProvince = control.get('originProvince');
    const destStreet = control.get('destStreet');
    const destCity = control.get('destCity');
    const destProvince = control.get('destProvince');

    const originAddress = control.get('originName');
    const destAddress = control.get('destName');

    let originAddressStr = [originStreet.value, originCity.value, originProvince.value].join('');
    let destAddressStr = [destStreet.value, destCity.value, destProvince.value].join('');

    /**
     * cases to check:
     * case1: "Agoho st." and "Agoho st"
     * case2: "Agoho  st" and "Agoho st"*
     * case3: "Agoho st " and "Agoho st"*
     * case4: "Agoho St" and "Agoho st"*
     * case4: "Agoho Street" and "Agoho st"
     */

    // satisfying cases 2-4
    originAddressStr = originAddressStr.trim().replace(/\s/g, '').toLowerCase();
    destAddressStr = destAddressStr.trim().replace(/\s/g, '').toLowerCase();

    if (originAddressStr.length >= userIntentAddressCharCount &&
      destAddressStr.length >= userIntentAddressCharCount &&
        originAddressStr === destAddressStr) {

      // add generic error
      originAddress.setErrors({ isSameLocation: true });
      destAddress.setErrors({ isSameLocation: true });

      originAddress.markAsTouched();
      destAddress.markAsTouched();
    } else {
      originAddress.markAsUntouched();
      destAddress.markAsUntouched();
    }

    return null;
};
