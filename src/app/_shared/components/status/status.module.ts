import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StatusApprovedComponent } from '@shared/components/status/status-approved/status-approved.component';
import { StatusDeniedComponent } from '@shared/components/status/status-denied/status-denied.component';
import { StatusExpiredComponent } from '@shared/components/status/status-expired/status-expired.component';
import { StatusPendingComponent } from '@shared/components/status/status-pending/status-pending.component';
import { MaterialModule } from '@shared/modules/material/material.module';
import { StatusSuspendedComponent } from './status-suspended/status-suspended.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    StatusApprovedComponent,
    StatusDeniedComponent,
    StatusExpiredComponent,
    StatusPendingComponent,
    StatusSuspendedComponent
  ],
  declarations: [
    StatusApprovedComponent,
    StatusDeniedComponent,
    StatusExpiredComponent,
    StatusPendingComponent,
    StatusSuspendedComponent
  ]
})
export class StatusModule {}
