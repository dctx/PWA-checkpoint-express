import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StatusModule } from '@shared/components/status/status.module';
import { MaterialModule } from '@shared/modules/material/material.module';

import { CheckStatusRoutingModule } from './check-status-routing.module';
import { CheckStatusComponent } from './check-status.component';
import { StatusInvalidDialogComponent } from './status-invalid-dialog/status-invalid-dialog.component';
import { InputTrimModule } from "ng2-trim-directive";


@NgModule({
  declarations: [
    CheckStatusComponent,
    StatusInvalidDialogComponent,
  ],
  imports: [
    CommonModule,
    CheckStatusRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    StatusModule,
    InputTrimModule
  ],
})
export class CheckStatusModule {}
