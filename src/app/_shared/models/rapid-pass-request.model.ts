export interface RapidPassRequestModel {
  aporType?: string;
  passType?: 'VEHICLE' | 'INDIVIDUAL';
  firstName?: string;
  middleName?: string;
  lastName?: string;
  suffix?: string;
  company?: string;
  idType?: string;
  identifierNumber?: string;
  email?: string;
  mobileNumber?: string;
  originName?: string;
  originStreet?: string;
  originCity?: string;
  originProvince?: string;
  destName?: string;
  destStreet?: string;
  destCity?: string;
  destProvince?: string;
  remarks?: string;
}
