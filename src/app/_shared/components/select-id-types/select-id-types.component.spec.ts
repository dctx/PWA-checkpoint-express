import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectIdTypesComponent } from './select-id-types.component';

describe('SelectIdTypesComponent', () => {
  let component: SelectIdTypesComponent;
  let fixture: ComponentFixture<SelectIdTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectIdTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectIdTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
