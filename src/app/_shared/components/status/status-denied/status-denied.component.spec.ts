import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusDeniedComponent } from './status-denied.component';

describe('StatusDeniedComponent', () => {
  let component: StatusDeniedComponent;
  let fixture: ComponentFixture<StatusDeniedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusDeniedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusDeniedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
