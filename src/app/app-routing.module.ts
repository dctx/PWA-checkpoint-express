import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from '@shared/services/can-deactivate.service';
import { FeedbackComponent } from './feedback/feedback.component'

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'onboarding',
    loadChildren: () => import('./onboarding/onboarding.module').then(m => m.OnboardingModule)
  },
  {
    path: 'registration',
    redirectTo: '',
  },
  {
    path: 'registration/:path',
    redirectTo: '',
  },
  {
    path: 'onboarding/:path',
    redirectTo: '',
  },
  {
    path: 'check-status',
    loadChildren: () => import('./check-status/check-status.module').then(m => m.CheckStatusModule)
  },
  {
    path: 'qcp-feedback',
    component: FeedbackComponent,
  }
];

@NgModule({
  providers: [
    CanDeactivateGuard
  ],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
