import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { AccessPassModel } from '@shared/models/access-pass.model';
import { ApiService } from '@shared/services/api.service';
import { Observable, of, timer } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AsyncValidators {

  constructor(private apiService: ApiService) {
  }

  existingReferenceId(label: string) {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      if (!control.dirty || !control.value) {
        return of(null);
      }
      return timer(800).pipe(
        switchMap(() => fromPromise(this.apiService.checkStatus(control.value))),
        catchError(() => of(null)),
        map((result: AccessPassModel) => {
          // if no records found, or if existing record is neither APPROVED or PENDING, mark as valid
          if (!result || result.status !== 'APPROVED' && result.status !== 'PENDING') {
            return null;
          }

          // if existing record is already expired, mark input as valid
          if (new Date() >= new Date(result.validUntil)) {
            return null;
          }

          // otherwise, mark as invalid
          return {
            existingReferenceId: {
              message: `Multiple active applications with the same ${ label } is not allowed.`
            }
          };
        })
      );
    };
  }

}
