import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { NavigationService } from '@shared/services/navigation.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  public pageTitle: string;
  public showToolbar: boolean;
  public transparentToolbar: boolean;

  constructor(private navigationService: NavigationService, private titleService: Title, private location: Location) {
    this.navigationService.currentTitle.subscribe(data => {
      if (data) {
        this.pageTitle = data.title || 'RapidPass.ph';
        this.showToolbar = data.toolbar;
        this.transparentToolbar = !this.pageTitle || !this.pageTitle.length;
      }
    });
  }

  public back(): void {
    this.location.back();
  }

}
