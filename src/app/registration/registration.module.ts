import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration-routing.module';
import { MaterialModule } from '@shared/modules/material/material.module';
import { VehicleRegistrationFormComponent } from './vehicle-registration-form/vehicle-registration-form.component';
import { IndividualRegistrationFormComponent } from './individual-registration-form/individual-registration-form.component';
import { SelectWorkTypesComponent } from '@shared/components/select-work-types/select-work-types.component';
import { SelectIdTypesComponent } from '@shared/components/select-id-types/select-id-types.component';
import { TrimDirective } from '@shared/directives/trim.directive';

@NgModule({
  declarations: [
    RegistrationComponent,
    VehicleRegistrationFormComponent,
    IndividualRegistrationFormComponent,
    SelectWorkTypesComponent,
    SelectIdTypesComponent,
    TrimDirective,
  ],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ]
})
export class RegistrationModule { }
