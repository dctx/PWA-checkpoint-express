import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '@shared/services/api.service';

@Component({
  selector: 'app-select-work-types',
  templateUrl: './select-work-types.component.html',
  styleUrls: ['./select-work-types.component.scss']
})
export class SelectWorkTypesComponent implements OnInit {

  public searchKey: string;

  public filteredItems: any;

  public workTypes: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<SelectWorkTypesComponent>,
    public apiService: ApiService
  ) {
  }

  public ngOnInit(): void {
    this.workTypes = this.apiService.getWorkTypes();
    this.onFilterItem(''); // when you fetch collection from server.
  }

  public onFilterItem(value: any) {
    if (!value) {
      this.filteredItems = this.workTypes;
    } // when nothing has typed
    this.filteredItems = this.workTypes
      .filter(item => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1);
  }

  public btnSelectThis(workType: any) {
    const selectedWorkType = {
      code: workType.code,
      label: workType.label
    };
    this.dialogRef.close(selectedWorkType);
  }

}
