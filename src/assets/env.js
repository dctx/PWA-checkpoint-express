(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["apiUrl"] = "https://rapidpass-api-stage.azurewebsites.net";
  window["env"]["debug"] = true;
})(this);
