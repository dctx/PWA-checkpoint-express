import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusExpiredComponent } from './status-expired.component';

describe('StatusExpiredComponent', () => {
  let component: StatusExpiredComponent;
  let fixture: ComponentFixture<StatusExpiredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusExpiredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusExpiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
