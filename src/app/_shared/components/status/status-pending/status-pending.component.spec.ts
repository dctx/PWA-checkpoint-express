import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusPendingComponent } from './status-pending.component';

describe('StatusPendingComponent', () => {
  let component: StatusPendingComponent;
  let fixture: ComponentFixture<StatusPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
