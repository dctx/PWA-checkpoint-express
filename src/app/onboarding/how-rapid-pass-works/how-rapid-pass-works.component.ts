import { Component, OnInit } from '@angular/core';
import { ApiService } from '@shared/services/api.service';

@Component({
  selector: 'app-how-rapid-pass-works',
  templateUrl: './how-rapid-pass-works.component.html',
  styleUrls: ['./how-rapid-pass-works.component.scss']
})
export class HowRapidPassWorksComponent implements OnInit {
  public workTypes: any;

  constructor(
    public apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.workTypes = this.apiService.getWorkTypes();
  }

}
