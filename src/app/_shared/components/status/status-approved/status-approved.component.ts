import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccessPassModel } from '@shared/models/access-pass.model';
import { ApiService } from '@shared/services/api.service';
import { environment } from '@env/environment';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';

@Component({
  selector: 'app-status-approved',
  templateUrl: './status-approved.component.html',
  styleUrls: ['./status-approved.component.scss']
})
export class StatusApprovedComponent implements OnInit {

  public data: AccessPassModel;

  public qrImage: any;

  public showQrCode: boolean;

  public processing: boolean;
  public downloadQRCodeLink: string;
  public expirationDate: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: any,
    private apiService: ApiService,
    private snackBar: MatSnackBar,
    private domSanitizer: DomSanitizer,
  ) {
    this.data = this.dialogData.result;
    this.expirationDate = moment(this.data.validUntil).format("MMM DD, YYYY");
    this.downloadQRCodeLink = `${environment.apiUrl}/api/v1/registry/qr-codes/${this.data.controlCode}`;
  }

  public ngOnInit(): void {
  }

  public async viewQrCode(): Promise<void> {
    this.processing = true;
    try {
      const base64ImgStr = await this.apiService.getQrImage(this.data.referenceId);
      this.qrImage = `data:image/png;base64, ${base64ImgStr}`;
      this.qrImage = this.domSanitizer.bypassSecurityTrustUrl(this.qrImage);
      this.showQrCode = true;
    } catch (error) {
      this.snackBar.open('Something went wrong. Please Try Again.', 'Dismiss', { duration: 2000 });
    }
    this.processing = false;
  }

}
