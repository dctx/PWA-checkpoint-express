import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusApprovedComponent } from './status-approved.component';

describe('StatusApprovedComponent', () => {
  let component: StatusApprovedComponent;
  let fixture: ComponentFixture<StatusApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
