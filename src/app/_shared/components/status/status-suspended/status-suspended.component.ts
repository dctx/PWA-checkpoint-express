import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AccessPassModel } from '@shared/models/access-pass.model';

@Component({
  selector: 'app-status-suspended',
  templateUrl: './status-suspended.component.html',
  styleUrls: ['./status-suspended.component.scss']
})
export class StatusSuspendedComponent implements OnInit {

  public showApplySuccess: boolean;

  public data: AccessPassModel;

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: any,
    private router: Router,
    private dialogRef: MatDialogRef<StatusSuspendedComponent>
  ) {
    this.data = this.dialogData.result;
  }

  public ngOnInit(): void {
  }

  goHome() {
    this.router.navigateByUrl('');
    this.dialogRef.close(true); // true if nav to home
  }

}
