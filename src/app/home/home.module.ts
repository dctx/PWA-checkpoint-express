import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@shared/modules/material/material.module';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { LandingComponent } from './landing/landing.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { FAQComponent } from './faq/faq.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactusComponent } from './contactus/contactus.component';

@NgModule({
  declarations: [
    HomeComponent,
    LandingComponent,
    PrivacyPolicyComponent,
    FAQComponent,
    AboutUsComponent,
    ContactusComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
