import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { RegistrationComponent } from './registration.component';
import { VehicleRegistrationFormComponent } from './vehicle-registration-form/vehicle-registration-form.component';
import { IndividualRegistrationFormComponent } from './individual-registration-form/individual-registration-form.component';
import { CanDeactivateGuard } from '@shared/services/can-deactivate.service';

const routes: Routes = [
  {
    path: '',
    component: RegistrationComponent,
    children: [
      {
        path: 'vehicle',
        component: VehicleRegistrationFormComponent,
        data: { title: 'RapidPass Application', toolbar: true },
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'individual',
        component: IndividualRegistrationFormComponent,
        data: { title: 'RapidPass Application', toolbar: true },
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule {}
