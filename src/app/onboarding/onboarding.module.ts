import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@shared/modules/material/material.module';
import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { HowRapidPassWorksComponent } from './how-rapid-pass-works/how-rapid-pass-works.component';
import { RemindersComponent } from './reminders/reminders.component';


@NgModule({
  declarations: [
    OnboardingComponent,
    HowRapidPassWorksComponent,
    RemindersComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    OnboardingRoutingModule
  ]
})
export class OnboardingModule { }
